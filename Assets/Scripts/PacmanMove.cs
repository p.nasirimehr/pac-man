﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class PacmanMove : MonoBehaviour
{
	/// <summary>
	/// Joystick object
	/// </summary>
	public Joystick joystick;

	/// <summary>
	/// Pac-man's movement speed
	/// </summary>
	public float speed = 0.1f;

	private float nextMoveDelay = 0.6f;

	/// <summary>
	/// Check if pac-man is moving
	/// </summary>
	private bool isMoving = false;

	/// <summary>
	/// Current destination according to dir
	/// </summary>
	private Vector2 dest = Vector2.zero;

	/// <summary>
	/// Next direction
	/// </summary>
	private Vector2 nextDir;

	/// <summary>
	/// Moving direction : {UP,DOWN,LEFT,RIGHT}
	/// </summary>
	private Vector2 dir;

	/// <summary>
	/// Temporary position used for storing pac-man's last frame position
	/// </summary>
	private Vector2 tempPos;

	/// <summary>
	/// Reference of game manager
	/// </summary>
	private GameManager manager;

	private AudioSource audioSource;

	public Vector3 Direction { get => dir; }

	public void Start()
	{
		isMoving = false;
		dest = transform.position;
		dir = Vector2.zero;
		nextDir = Vector2.zero;
		manager = FindObjectOfType<GameManager>();
		audioSource = GetComponent<AudioSource>();
	}

	/// <summary>
	/// Handling pac-man's movement in FixedUpdate
	/// </summary>
	public void FixedUpdate()
	{
		Vector2 p = Vector2.MoveTowards(transform.position, dest, speed);
		GetComponent<Rigidbody2D>().MovePosition(p);

		if (((Vector2)transform.position - dest).magnitude < 1.3)
		{
			HandleJoystick();
			HandleKeyboard();
		}
		if (HitDeadEnd(dir))
		{
			dir = nextDir;
			isMoving = false;
		}
		if (NextMoveValid())
		{
			dir = nextDir;
		}
		tempPos = transform.position;
		/* Changing animation according to movement direction*/
		GetComponent<Animator>().SetFloat("DirX", dir.x);
		GetComponent<Animator>().SetFloat("DirY", dir.y);
	}

	public void OnTriggerEnter2D(Collider2D other)
	{
		if (other.name == "pacdot" && other.gameObject.GetComponent<SpriteRenderer>().enabled)
		{
			other.gameObject.GetComponent<SpriteRenderer>().enabled = false;
			manager.Eatdot();
			if(!audioSource.isPlaying)
				audioSource.Play();
		}
		if (other.name == "powerup")
		{
			manager.SignalFrightened();
			Destroy(other.gameObject);
		}
	}

	public void OnDrawGizmos()
	{
		Vector2 pos = transform.position;
		Gizmos.DrawLine(pos + dir + dir / 10, pos + dir + dir * 10f);
		Gizmos.DrawLine(pos + dir / 6 + nextDir, pos + dir / 6 + nextDir * 2);
	}

	/// <summary>
	/// Check if pac-man's first move direction is valid
	/// </summary>
	/// <param name="dir"></param>
	/// <returns></returns>
	private bool Valid(Vector2 dir)
	{
		Vector2 pos = transform.position;
		RaycastHit2D hit = Physics2D.Linecast(pos + dir + dir / 10, pos + dir + dir * 10f);
		return (hit.collider);
	}

	/// <summary>
	/// Check if pac-man's second move direction is valid
	/// </summary>
	/// <returns></returns>
	private bool NextMoveValid()
	{
		Vector2 pos = transform.position;
		RaycastHit2D hit = Physics2D.Linecast(pos + dir * (1 - nextMoveDelay) + nextDir, pos + dir * (1 - nextMoveDelay) + nextDir * 2);
		if (hit)
		{
			//colliding with pacdocs
			return hit.collider.gameObject.layer == 0;
		}
		return false;
	}

	/// <summary>
	/// Check if pac-man has hit a dead-end
	/// </summary>
	/// <param name="dir"></param>
	/// <returns></returns>
	bool HitDeadEnd(Vector2 dir)
	{
		//TODO what is dir used for? how does this work?
		Vector2 pos = transform.position;
		return (tempPos - pos).magnitude < 0.01;
	}

	/// <summary>
	/// Handling joystick movements
	/// </summary>
	private void HandleJoystick()
	{
		float horizontalMove = joystick.Horizontal;
		float verticalMove = joystick.Vertical;
		if (Mathf.Abs(horizontalMove) >= .2f || Mathf.Abs(verticalMove) >= .2f)
		{ // move at all
			if (Mathf.Abs(horizontalMove) > Mathf.Abs(verticalMove))
			{ // move horizontaly
				if (horizontalMove > 0 && Valid(Vector2.right)) // move right
					nextDir = Vector2.right;
				else if (horizontalMove < 0 && Valid(-Vector2.right)) // move left
					nextDir = Vector2.left;
			}
			else if (Mathf.Abs(horizontalMove) <= Mathf.Abs(verticalMove))
			{// move vertically
				if (verticalMove > 0 && Valid(Vector2.up))
					nextDir = Vector2.up;
				else if (verticalMove < 0 && Valid(-Vector2.up))
					nextDir = Vector2.down;
			}
		}
	}

	/// <summary>
	/// Handling keyboard movement
	/// </summary>
	private void HandleKeyboard()
	{
		//Pac-man can decide which direction to go next, while moving
		if (isMoving)
		{
			if (Input.GetKey(KeyCode.UpArrow) && Valid(Vector2.up))
			{
				nextDir = Vector2.up;
			}
			else if (Input.GetKey(KeyCode.RightArrow) && Valid(Vector2.right))
			{
				nextDir = Vector2.right;
			}
			else if (Input.GetKey(KeyCode.DownArrow) && Valid(-Vector2.up))
			{
				nextDir = Vector2.down;
			}
			else if (Input.GetKey(KeyCode.LeftArrow) && Valid(-Vector2.right))
			{
				nextDir = Vector2.left;
			}
		}

		//current moving direction
		if (!isMoving)
		{
			if (Input.GetKey(KeyCode.UpArrow) && Valid(Vector2.up))
			{
				dir = Vector2.up;
			}
			else if (Input.GetKey(KeyCode.RightArrow) && Valid(Vector2.right))
			{
				dir = Vector2.right;
			}
			else if (Input.GetKey(KeyCode.DownArrow) && Valid(-Vector2.up))
			{
				dir = Vector2.down;
			}
			else if (Input.GetKey(KeyCode.LeftArrow) && Valid(-Vector2.right))
			{
				dir = Vector2.left;
			}
			isMoving = true;
		}

		dest = (Vector2)transform.position + dir;
	}

}
