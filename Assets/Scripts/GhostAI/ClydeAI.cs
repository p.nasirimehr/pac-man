﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System.Linq;

public class ClydeAI : GhostAI
{
	private Transform clydePos;

	public ClydeAI(GameManager manager, GameObject pacman, Transform scatter, Transform ClydePos, Transform boxExit)
		: base(manager, pacman, scatter, GhostState.InBox, boxExit)
	{
		this.clydePos = ClydePos;
	}

	public override Vector3 GetChaseTarget()
	{
		var target = pacman.transform.position;
		if ((target - clydePos.position).magnitude < 8)
			target = scatter.position;

		return target;
	}
}
