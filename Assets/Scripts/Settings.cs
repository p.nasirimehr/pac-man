﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class Settings
{
	public static readonly float FrightenedTimer  = 7f;

	public static readonly float StateChase  = 20f;

	public static readonly float StateScatter12  = 7f;

	public static readonly float StateScatter34  = 5f;

	public static readonly int ScorePacdot = 10;

	public static readonly int ScoreEnergizer = 50;

	public static readonly int ScoreGhost = 200;
}

