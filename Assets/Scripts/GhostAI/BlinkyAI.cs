﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System.Linq;

public class BlinkyAI : GhostAI
{
	public BlinkyAI(GameManager manager, GameObject pacman, Transform scatter, Transform boxExit)
		: base(manager, pacman, scatter, GhostState.Scatter, boxExit)
	{
	}

	public override Vector3 GetChaseTarget()
	{
		return pacman.transform.position;
	}
}
