﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System.Linq;

public enum GhostState
{
	InBox,
	ExitBox,
	Scatter,
	Chase,
}
