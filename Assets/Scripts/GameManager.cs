﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pathfinding;

using Unity.Mathematics;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public GameObject pacman;

	public Transform Blinky;

	public Transform Clyde;

	public int2[] NoUpNodes;

	public int2[] NoDownNodes;

	public Transform BoxExit;

	public Transform BlinkyScatter;
	public Transform ClydeScatter;
	public Transform InkyScatter;
	public Transform PinkyScatter;
	public Transform ghostBox;

	public Text scoreText;

	private PacmanMove pacmanClass;
	public GridGraph Graph { get; private set; }
	private Dictionary<GhostEnum, GhostAI> ghosts;

	public int EatenPacdots { get; private set; }

	public int Score { get; private set; }

	public Transform GhostBoxWaypoint { get => ghostBox; }

	public void Start()
	{
		Graph = Pathfinding.AstarData.active.data.FindGraphOfType(typeof(GridGraph)) as GridGraph;
		pacmanClass = pacman.GetComponent<PacmanMove>();
		EatenPacdots = 0;
		Application.targetFrameRate = 60;

		ghosts = new Dictionary<GhostEnum, GhostAI>(4);
		ghosts.Add(GhostEnum.Blinky, new BlinkyAI(this, pacman, BlinkyScatter, BoxExit));
		ghosts.Add(GhostEnum.Clyde, new ClydeAI(this, pacman, ClydeScatter, Clyde, BoxExit));
		ghosts.Add(GhostEnum.Inky, new InkyAI(this, pacman, Blinky, InkyScatter, BoxExit));
		ghosts.Add(GhostEnum.Pinky, new PinkyAI(this, pacman, PinkyScatter, BoxExit));
	}

	public void Update()
	{
		scoreText.text = Score.ToString();
	}

	/// <summary>
	/// Get the instance of a given ghost
	/// </summary>
	/// <param name="ghost">ghost type</param>
	/// <returns>GhostAI instance</returns>
	public GhostAI GetAI(GhostEnum ghost)
	{
		return ghosts[ghost];
	}

	/// <summary>
	/// Get the 4 neighbor of the given node seperatly
	/// </summary>
	/// <param name="current">center node</param>
	/// <param name="up"></param>
	/// <param name="right"></param>
	/// <param name="down"></param>
	/// <param name="left"></param>
	public void GetNeighbors(GridNodeBase current, out GridNodeBase up, out GridNodeBase right, out GridNodeBase down, out GridNodeBase left)
	{
		up = Graph.GetNode(current.XCoordinateInGrid, current.ZCoordinateInGrid + 1);
		down = Graph.GetNode(current.XCoordinateInGrid, current.ZCoordinateInGrid - 1);
		left = Graph.GetNode(current.XCoordinateInGrid - 1, current.ZCoordinateInGrid);
		right = Graph.GetNode(current.XCoordinateInGrid + 1, current.ZCoordinateInGrid);
	}

	/// <summary>
	/// Get the 4 neighbor of the given node
	/// </summary>
	/// <param name="current">center node</param>
	/// <returns>List of neighbors along with their direction</returns>
	public List<Tuple<Vector3, GridNodeBase>> GetNeighbors(GridNodeBase current)
	{
		var res = new List<Tuple<Vector3, GridNodeBase>>
		{
			new Tuple<Vector3, GridNodeBase>(
				Vector3.up,
				Graph.GetNode(current.XCoordinateInGrid, current.ZCoordinateInGrid + 1)),

			new Tuple<Vector3, GridNodeBase>(
				Vector3.left,
				Graph.GetNode(current.XCoordinateInGrid - 1, current.ZCoordinateInGrid)),

			new Tuple<Vector3, GridNodeBase>(
				Vector3.down,
				Graph.GetNode(current.XCoordinateInGrid, current.ZCoordinateInGrid - 1)),

			new Tuple<Vector3, GridNodeBase>(
				Vector3.right,
				Graph.GetNode(current.XCoordinateInGrid + 1, current.ZCoordinateInGrid))
		};

		return res;
	}

	public bool CanGo(GridNodeBase current, Vector3 dir)
	{
		if (dir == Vector3.up)
			return CanGoUp(current);
		else if (dir == Vector3.down)
			return CanGoDown(current);
		else
			return true;
	}

	public bool CanGoUp(GridNodeBase current)
	{
		return !NoUpNodes.Contains(
			new int2 { x = current.XCoordinateInGrid, y = current.ZCoordinateInGrid });
	}

	public bool CanGoDown(GridNodeBase current)
	{
		return !NoDownNodes.Contains(
			new int2 { x = current.XCoordinateInGrid, y = current.ZCoordinateInGrid });
	}

	/// <summary>
	/// Called by pacman after eating normal pacdots
	/// </summary>
	public void Eatdot()
	{
		EatenPacdots++;
		Score += Settings.ScorePacdot;
	}

	/// <summary>
	/// Called by pacman when it eats an energizer
	/// </summary>
	public void SignalFrightened()
	{
		Score += Settings.ScoreEnergizer;
		foreach (var item in ghosts)
		{
			item.Value.SignalFrightened();
		}
	}

	/// <summary>
	/// Called by ghosts when it is eaten
	/// </summary>
	public void SignalEatenByPacman()
	{
		//TODO ghost score multplier?
		Score += Settings.ScoreGhost;
	}

	public void OnDrawGizmos()
	{
		if (Graph != null)
		{
			foreach (var item in NoUpNodes)
			{
				Gizmos.DrawSphere((Vector3)Graph.GetNode(item.x, item.y).position, 0.3f);
			}
			foreach (var item in NoDownNodes)
			{
				Gizmos.DrawSphere((Vector3)Graph.GetNode(item.x, item.y).position, 0.3f);
			}
		}

		if (pacmanClass)
		{
			Vector3 target = pacman.transform.position;
			Vector3 pacmanTwoTiles = target + pacmanClass.Direction * 2;
			Vector3 fromBlinky = pacmanTwoTiles - Blinky.position;
			target = fromBlinky * 2 + Blinky.position;

			Gizmos.color = Color.blue;
			Gizmos.DrawLine(Blinky.position, target);
			Gizmos.color = Color.yellow;
			Gizmos.DrawLine(Clyde.position, pacman.transform.position);

			//Pinky target
			Gizmos.color = new Color(1, 0.68f, 0.78f);
			Gizmos.DrawSphere(pacman.transform.position + 4 * pacmanClass.Direction, 0.5f);

			//Blinky target
			Gizmos.color = Color.cyan;
			Gizmos.DrawSphere(target, 0.5f);
		}
	}
}

