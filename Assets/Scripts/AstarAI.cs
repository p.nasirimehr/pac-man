﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class AstarAI : MonoBehaviour
{
	//The point to move to
	public Transform target;

	private Seeker seeker;

	//The calculated path
	public Path path;

	//The AI's speed per second
	public float speed = 2;

	//The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 0.02f;

	public float updateTime = 1f;
	private float remainingTime = 1f;
	private bool updatePath;

	//The waypoint we are currently moving towards
	private int currentWaypoint = 0;

	public void Start()
	{
		seeker = GetComponent<Seeker>();

		//Start a new path to the targetPosition, return the result to the OnPathComplete function
		seeker.StartPath(transform.position, target.position, OnPathComplete);
	}

	public void OnPathComplete(Path p)
	{
		Debug.Log("Yay, we got a path back. Did it have an error? " + p.error);
		if (!p.error)
		{
			path = p;
			//Reset the waypoint counter
			currentWaypoint = 0;
		}
	}

	public void Update()
	{
		remainingTime -= Time.deltaTime;
		if (remainingTime < 0)
		{
			remainingTime = updateTime;
			updatePath = true;
		}
	}

	public void FixedUpdate()
	{
		if (path == null)
		{
			//We have no path to move after yet
			return;
		}

		if (currentWaypoint >= path.vectorPath.Count)
		{
			Debug.Log("End Of Path Reached");
			return;
		}

		//Direction to the next waypoint
		Vector2 p = Vector2.MoveTowards(transform.position, path.vectorPath[currentWaypoint], speed);
		GetComponent<Rigidbody2D>().MovePosition(p);

		// TODO for ghost animation
		//Vector2 dir = path.vectorPath[currentWaypoint] - transform.position;
		//GetComponent<Animator>().SetFloat("DirX", dir.x);
		//GetComponent<Animator>().SetFloat("DirY", dir.y);

		//Check if we are close enough to the next waypoint
		//If we are, proceed to follow the next waypoint
		if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
		{
			currentWaypoint++;
			if (updatePath)
			{
				seeker.StartPath(transform.position, target.position, OnPathComplete);
				updatePath = false;
				remainingTime = updateTime;
			}
		}
	}
}