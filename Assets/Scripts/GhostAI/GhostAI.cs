﻿using UnityEngine;
using Pathfinding;
using System.Linq;

public abstract class GhostAI
{
	protected GameManager manager;

	protected GameObject pacman;

	protected Transform scatter;

	protected GhostState lastState;

	protected Transform boxExit;

	protected float stateTimer;

	protected int stateStage;

	public GhostState State { get; protected set; }

	public bool Frightened { get; protected set; }

	public float FrightenedTimer { get; protected set; }

	protected GhostAI(GameManager manager, GameObject pacman, Transform scatter, GhostState state, Transform boxExit)
	{
		this.manager = manager;
		this.pacman = pacman;
		this.scatter = scatter;
		this.State = state;
		this.boxExit = boxExit;
		Frightened = false;
		stateTimer = Settings.StateScatter12;
		stateStage = 0;
	}

	public void GetNextNode(Vector3 lastDir, GridNodeBase node, out Vector3 nextDir, out GridNodeBase nextNode)
	{
		Vector3 target = GetTargetNode(lastDir, node);

		var neighbors = manager.GetNeighbors(node);

		var available = neighbors
			.Select(r => new
			{
				Dir = r.Item1,
				Node = r.Item2,
				Dist = Vector3.Distance((Vector3)r.Item2.position, target)
			}).Where(r => r.Node.Walkable);

		//force reverse
		if (State != lastState && (lastState == GhostState.Chase || lastState == GhostState.Scatter) && State != GhostState.ExitBox)
		{
			available = available.Where(r => r.Dir == lastDir * -1);
		}
		else if (State == GhostState.Chase && !Frightened)
		{
			available = available.Where(r => r.Dir != lastDir * -1 && manager.CanGo(node, r.Dir));

			//Debug.Assert(available.Count() > 0,
			//	$"t:{GetType()}, " +
			//	$"s:{state}, " +
			//	$"ls: {lastState}, " +
			//	$"ld: {lastDir}, " +
			//	$"ln: {node}, " +
			//	$"p: chase");
		}
		else if (State == GhostState.Scatter && !Frightened)
		{
			available = available.Where(r => r.Dir != lastDir * -1 && manager.CanGo(node, r.Dir));
		}
		else if (State == GhostState.InBox)
		{
			available = available.Where(r => r.Dir == Vector3.up || r.Dir == Vector3.down);
		}
		else if (State == GhostState.ExitBox)
		{
			available = available.Where(r => r.Dir != lastDir * -1 && manager.CanGo(node, r.Dir));
		}
		else if (Frightened)
		{
			//no going back and no going into the ghost house
			available = available.Where(r =>
						(r.Dir != lastDir * -1)
						&& (r.Dir != Vector3.down || manager.CanGoDown(node)));
			//randomly pick one
			var i = Random.Range(0, available.Count());
			available = available.Skip(i).Take(1);
		}

		lastState = State;

		var list = available.ToList();
		var minDist = list.Min(r => r.Dist);
		var next = list.First(r => r.Dist == minDist);

		nextNode = next.Node;
		nextDir = next.Dir;
	}

	public Vector3 GetTargetNode(Vector3 lastDir, GridNodeBase node)
	{
		switch (State)
		{
			case GhostState.InBox: return GetInBoxTarget(lastDir, node);
			case GhostState.ExitBox: return boxExit.position;
			case GhostState.Chase: return GetChaseTarget();
			case GhostState.Scatter: return scatter.position;
			default: return Vector3.negativeInfinity;
		}
	}

	public Vector3 GetInBoxTarget(Vector3 lastDir, GridNodeBase node)
	{
		manager.GetNeighbors(node, out var u, out _, out var d, out _);
		if (lastDir == Vector3.up)
			if (u.Walkable)
				return (Vector3)u.position;
			else
				return (Vector3)d.position;
		else
			if (d.Walkable)
			return (Vector3)d.position;
		else
			return (Vector3)u.position;
	}

	public abstract Vector3 GetChaseTarget();

	/// <summary>
	/// Called by MonoBeehaviour to advance timer based stateMachine
	/// </summary>
	/// <param name="deltaTime">The completion time in seconds since the last frame</param>
	public void ProgressTime(float deltaTime)
	{
		//State machine manager
		if (Frightened)
		{
			FrightenedTimer -= deltaTime;
			if (FrightenedTimer <= 0)
			{
				Frightened = false;
			}
		}
		//state timer run when not frightened and not in box
		else if (State != GhostState.ExitBox && State != GhostState.InBox)
		{
			stateTimer -= deltaTime;
			if (stateTimer <= 0)
			{
				if (State == GhostState.Scatter)
				{
					State = GhostState.Chase;
					stateTimer = Settings.StateChase;
				}
				else if (State == GhostState.Chase)
				{
					if (stateStage < 2)
					{
						State = GhostState.Scatter;
						stateTimer = Settings.StateScatter12;
					}
					else if (stateStage < 4)
					{
						State = GhostState.Scatter;
						stateTimer = Settings.StateScatter34;
					}
					else
					{
						//permenent chase mode, dont go back to scatter
						State = GhostState.Chase;
						stateTimer = float.PositiveInfinity;
					}
				}
			}
		}
	}

	/// <summary>
	/// Called by the game manger to signal that the ghost needs to leave the ghost house.
	/// Will not have any effect if the ghost's state is not InBox
	/// </summary>
	public void SignalExitBox()
	{
		if (State == GhostState.InBox)
			State = GhostState.ExitBox;
	}

	/// <summary>
	/// Called by the GhostMove to signal that the ghost has fully exited the ghost house.
	/// Will not have any effect if the ghost's state is not ExitBox
	/// </summary>
	public void SignalFinishExit()
	{
		if (State == GhostState.ExitBox)
		{
			State = GhostState.Scatter;
			//stateTimer = 7f;
		}
	}

	/// <summary>
	/// Called by the game manger to signal that the ghost is frieghtened.
	/// Will reset the timer if called while still frightened
	/// </summary>
	public void SignalFrightened()
	{
		Frightened = true;
		FrightenedTimer = Settings.FrightenedTimer;
	}

	/// <summary>
	/// Called by GhotMove when pacman eats the ghost
	/// State will go to exit box and ends frieghtened
	/// </summary>
	public GridNodeBase SignalEaten(Vector3 pos)
	{
		//TODO has to move to ghost house instead of teleport
		Frightened = false;
		State = GhostState.ExitBox;
		return (GridNodeBase)manager.Graph.GetNearest(pos, NNConstraint.Default).node;
	}
}
