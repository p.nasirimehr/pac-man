﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System.Linq;

public class InkyAI : GhostAI
{
	private Transform blinky;

	private PacmanMove pacmanClass;

	public InkyAI(GameManager manager, GameObject pacman, Transform blinky, Transform scatter, Transform boxExit)
		: base(manager, pacman, scatter, GhostState.InBox, boxExit)
	{
		this.blinky = blinky;
		pacmanClass = pacman.GetComponent<PacmanMove>();
	}

	public override Vector3 GetChaseTarget()
	{
		Vector3 target = pacman.transform.position;
		Vector3 pacmanTwoTiles = target + pacmanClass.Direction * 2;
		Vector3 fromBlinky = pacmanTwoTiles - blinky.position;

		return fromBlinky * 2 + blinky.position;
	}
}
