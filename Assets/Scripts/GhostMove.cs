﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Pathfinding;

public class GhostMove : MonoBehaviour
{
	//The AI's speed per second
	public float speed = 1f;

	//The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 0.005f;

	public GameManager gameManager;

	public GhostEnum ghostEnum;

	public Sprite frightenedSprite;

	private Vector3 direction;
	private GridNodeBase targetNode;
	private GridGraph graph;
	private GhostAI ghostAI;

	public void Start()
	{
		graph = Pathfinding.AstarData.active.data.FindGraphOfType(typeof(GridGraph)) as GridGraph;

		var n = (GridNodeBase)graph.GetNearest(transform.position).node;
		ghostAI = gameManager.GetAI(ghostEnum);
		//targetNode = graph.GetNode(n.XCoordinateInGrid - 1, n.ZCoordinateInGrid);
		//direction = Vector3.left;
		targetNode = graph.GetNode(n.XCoordinateInGrid, n.ZCoordinateInGrid);
		direction = Vector3.zero;
	}

	public void FixedUpdate()
	{
		//slower when frightened
		var tmpSpeed = ghostAI.Frightened ? speed * 0.7f : speed;

		//Direction to the next waypoint
		Vector2 p = Vector2.MoveTowards(transform.position, (Vector3)targetNode.position, tmpSpeed);
		GetComponent<Rigidbody2D>().MovePosition(p);

		//desicion making
		if (Vector3.Distance(transform.position, (Vector3)targetNode.position) < nextWaypointDistance)
		{
			Vector3 tmpDir;
			GridNodeBase tmpNode;
			ghostAI.GetNextNode(direction, targetNode, out tmpDir, out tmpNode);
			direction = tmpDir;
			targetNode = tmpNode;
		}

		//For ghost animation
		GetComponent<Animator>().SetFloat("DirX", direction.x);
		GetComponent<Animator>().SetFloat("DirY", direction.y);
		GetComponent<Animator>().SetBool("Frightened", ghostAI.Frightened);
		GetComponent<Animator>().SetFloat("FrightenedTimer", ghostAI.FrightenedTimer);
	}

	/*Handling State machine.*/
	public void Update()
	{
		ghostAI.ProgressTime(Time.deltaTime);

		if (ghostEnum == GhostEnum.Clyde && gameManager.EatenPacdots > 100 && ghostAI.State == GhostState.InBox)
		{
			ghostAI.SignalExitBox();
		}
		if (ghostEnum == GhostEnum.Inky && gameManager.EatenPacdots > 30 && ghostAI.State == GhostState.InBox)
		{
			ghostAI.SignalExitBox();
		}
	}

	public void OnDrawGizmos()
	{
		Vector3 pos = transform.position;

		Gizmos.DrawLine(pos, pos + direction);
		Gizmos.color = Color.clear;
		if (ghostAI?.State == GhostState.Chase)
			Gizmos.color = Color.red;
		if (ghostAI?.State == GhostState.Scatter)
			Gizmos.color = Color.green;
		Gizmos.DrawSphere(transform.position, .3f);
	}

	public void OnTriggerEnter2D(Collider2D co)
	{
		//TODO consider fireghtened state
		if (co.name == "pacman")
		{
			if (!ghostAI.Frightened)
			{
				//TODO add lives? animation?
				Destroy(co.gameObject);
				SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
			}
			else
			{
				//TODO score
				//transform.SetPositionAndRotation(gameManager.GhostBoxWaypoint.position, Quaternion.identity);
				transform.position = gameManager.GhostBoxWaypoint.position;
				targetNode = ghostAI.SignalEaten(transform.position);

				Vector3 tmpDir;
				GridNodeBase tmpNode;
				ghostAI.GetNextNode(Vector3.zero, targetNode, out tmpDir, out tmpNode);
				direction = tmpDir;
				targetNode = tmpNode;

				//For ghost animation
				GetComponent<Animator>().SetFloat("DirX", direction.x);
				GetComponent<Animator>().SetFloat("DirY", direction.y);
				GetComponent<Animator>().SetBool("Frightened", ghostAI.Frightened);
				GetComponent<Animator>().SetFloat("FrightenedTimer", ghostAI.FrightenedTimer);

			}
		}

		if (co.name == "box exit" && ghostAI.State == GhostState.ExitBox)
		{
			ghostAI.SignalFinishExit();
		}
	}
}
