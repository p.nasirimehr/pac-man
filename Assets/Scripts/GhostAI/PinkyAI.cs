﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System.Linq;

public class PinkyAI : GhostAI
{
	private PacmanMove pacmanClass;

	public PinkyAI(GameManager manager, GameObject pacman, Transform scatter, Transform boxExit)
		: base(manager, pacman, scatter, GhostState.ExitBox, boxExit)
	{
		pacmanClass = pacman.GetComponent<PacmanMove>();
	}

	public override Vector3 GetChaseTarget()
	{
		var target = pacman.transform.position;

		var dir = pacmanClass.Direction;
		return target + dir * 4;
	}
}
